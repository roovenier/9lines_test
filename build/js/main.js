var skillCounter = {
  mySkillValue: 0,
  init: function() {
    var $self = this;
    var maxSkillValue = $('.skills-item').toArray().reduce(function(prev, current) {
      return prev + parseInt($(current).find('.skills-input').data('skill'));
    }, 0);

    $('.skills-item').each(function(i, elem) {
      $self.mySkillValue += ($(elem).find('.skills-input').is(':checked')) ? parseInt($(elem).find('.skills-input').data('skill')) : 0;
    });

    this.makeRotation(maxSkillValue);

    var skillValueAnim = new CountUp('js-level-score', 0, this.mySkillValue);
    skillValueAnim.start();

    $('.skills-input').on('change', function() {
      var skillValue = parseInt($(this).data('skill'));
      $self.mySkillValue += this.checked ? skillValue : skillValue * -1;

      $self.makeRotation(maxSkillValue);

      skillValueAnim.update($self.mySkillValue);
    });
  },
  makeRotation: function(maxSkillValue) {
    var percent = this.mySkillValue / maxSkillValue * 100;
    var rotateValue = percent / 100 * 180;
    $('.js-level-arrow').css({'transform' : 'rotate('+ rotateValue +'deg)'});
  }
}

$(document).ready(function() {

    // Если в проекте используются встроенные js-плагины от Foundation, разкомментировать
    // $(document).foundation();

    skillCounter.init();

});